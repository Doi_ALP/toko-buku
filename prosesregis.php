<?php
require_once('koneksi.php');

$username = $_POST["username"];
$password = $_POST["password"];
$repassword = $_POST["repassword"];
$nama = $_POST["nama"];
$email = $_POST["email"];
$aturan = "member";

//echo $password." - ".$repassword;

//menggunakan hash enkripsi
if($password==$repassword)
    $passhash = md5($password);
else
    die("Password dan Repassword harus sama..");

// prepare and bind

try{
    $stmt = $conn->prepare('INSERT INTO pengguna (Username,Kunci,Aturan,Nama,Email) VALUES (?, ?, ?, ?, ?)');
    $stmt->bind_param("sssss", $username, $passhash, $aturan,$nama,$email);
    $stmt->execute();
    header("Location: /RPLGroupA/registrasi.php?pesan=proses registrasi berhasil");
    die();
}
catch(Exception $e){
    $error = $e->getMessage();
}
finally{
    $stmt->close();
    $conn->close();
}


?>