<?php
require_once('/pageheader.php');
?>

<ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Home</a>
        </li>
        <li class="breadcrumb-item active">Halaman Login</li>
      </ol>
      <div class="row">
        <div class="col-6">
          
        <h1>Login</h1>
        <form action="proseslogin.php" method="POST">
            <div class="form-group">
                <label for="username">Username:</label>
                <input type="text" class="form-control" name="username">
            </div>
            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" class="form-control" name="password">
            </div>
        <button type="submit" class="btn btn-default">Submit</button>
        </form><br>
        <?php 
          if(isset($_GET["pesan"]) && $_GET["pesan"]!=""){
            echo "<span class='alert alert-danger'>".$_GET["pesan"]."</span>";
          }
          ?>
        </div>
</div>

<?php
require_once('/pagefooter.php');
?>