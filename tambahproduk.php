<?php 


require_once('../coding/pageheader.php');
?>

<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="index.php">Home</a>
    </li>
    <li class="breadcrumb-item active">Tambah Produk</li>
</ol>
<div class="row">
    <div class="col-6">
    <?php 
          if(isset($_GET["pesan"]) && $_GET["pesan"]!=""){
            echo "<span class='alert alert-success'>".$_GET["pesan"]."</span>";
          }
          ?>
        <h3>Form Tambah Produk</h3><br>
        <form action="prosestambah.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="namaproduk">Kode Buku :</label>
            <input type="text" class="form-control" name="kode_buku">
        </div>
        <div class="form-group">
            <label for="quantity">Judul Buku :</label>
            <input type="text" class="form-control" name="judul_buku">
        </div>
        <div class="form-group">
            <label for="hargabeli">Pengarang :</label>
            <input type="text" class="form-control" name="pengarang">
        </div>
        <div class="form-group">
            <label for="hargajual">Penerbit :</label>
            <input type="text" class="form-control" name="penerbit">
        </div>
        <div class="form-group">
            <label for="hargajual">Tahun Terbit :</label>
            <input type="text" class="form-control" name="tahun_terbit">
        </div>
        <div class="form-group">
            <label for="fileToUpload">Gambar :</label>
            <input type="file" name="fileToUpload">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
    </div>
</div>

<?php
require_once('../coding/pagefooter.php');
?>